const db = require("./connection");
const dateFormat = require("dateformat");

const Controllers = {
    getCategories: async (user_id) => {
        let SQL = `SELECT * FROM categories
                JOIN icons ON categories.icon_id = icons.icon_id
                WHERE categories.user_id = 0 or categories.user_id = ?`;

        const [rows, fields] = await db.execute(SQL, [user_id]);

        return rows;
    },

    getCatIcons: async () => {
        //SQL
        let SQL = `SELECT *
                    FROM icons`

        //Query

        const [rows, fields] = await db.execute(SQL);

        return rows
    },

    registerUser: async (mail,password) => {
        //SQL
        let SQL = `INSERT INTO users (email, password)
                    VALUES (?, ?)`

        //Query
        const [rows, fields] = await db.execute(SQL,[mail,password]);
        return rows
    },

    login: async (mail,password) => {
        //SQL
        let SQL = `SELECT *
                    FROM users
                    WHERE email = ?
                      AND password = ?`

        //Query
        const [rows, fields] = await db.execute(SQL,[mail,password]);
        return rows
    },

    addIcon: async (name,color) => {
        //SQL
        let SQL = `INSERT INTO icons (icon_name, icon_color)
                    VALUES (?, ?)`

        //Query
        const [rows, fields] = await db.execute(SQL,[name,color]);
        return rows
    },

    addCategory: async (cat_name,type,icon_id,user_id) => {
        //SQL
        let SQL = `INSERT INTO categories (cat_name, type, icon_id, user_id)
                    VALUES (?, ?, ?, ?)`

        //query
        const [rows, fields] = await db.execute(SQL,[cat_name,type,icon_id,user_id]);
        return rows
    },
    addAmount: async (table,cat_id,amount,memo,user_id) => {
        //SQL
        let SQL = `INSERT INTO ${table} (cat_id, amount, memo, user_id)
                    VALUES (?, ?, ?, ?)`

        //query
        const [rows, fields] = await db.execute(SQL,[cat_id,amount,memo,user_id]);
        return rows
    },
    getAmount: async (user_id) => {
        //SQL
        let SQL = `SELECT tr.ID,
                           tr.cat_id,
                           tr.amount,
                           tr.memo,
                           tr.user_id,
                           tr.type_amount,
                           tr.time,
                           cat.cat_name,
                           i.icon_id,
                           i.icon_name,
                           i.icon_color
                    FROM (
                             SELECT *
                             FROM expenses ex
                             WHERE ex.user_id = ?
                             UNION
                             SELECT *
                             FROM income inc
                             WHERE inc.user_id = ?
                         ) AS tr
                             LEFT JOIN categories cat ON tr.cat_id = cat.id
                             LEFT JOIN icons i ON cat.icon_id = i.icon_id
                    ORDER BY
                        TIME DESC;`

        //Query
        const [rows, fields] = await db.execute(SQL,[user_id,user_id]);

        return rows

    },
    getBalance: async (user_id) => {
        //SQL
        let SQL = `SELECT type_amount, SUM(amount) total
                    FROM (SELECT type_amount, amount
                          FROM expenses
                          WHERE user_id = ?
                          UNION ALL
                          SELECT type_amount, amount
                          FROM income
                          WHERE user_id = ?) t
                    GROUP BY type_amount;`

        //Query
        const [rows, fields] = await db.execute(SQL,[user_id,user_id]);
        return rows

    }


}

module.exports = Controllers