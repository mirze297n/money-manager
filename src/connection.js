const mysql = require('mysql2/promise')
const connection = mysql.createPool({
    host: 'localhost',
    user: 'root',
    password:'',
    database: 'money_manager',
    connectionLimit: 10
})

module.exports = connection
