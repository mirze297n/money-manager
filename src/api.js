const db = require('./connection')
const dateFormat = require("dateformat")
const controllers = require("./controllers")

const startAPI = (app) => {


//POST

    app.post('/api/user-register', (req, res) => {
        //variables
        let mail = req.body.email
        let password = req.body.password

        controllers.registerUser(mail, password).then(function (rows) {
            res.json({
                msg: 'success',
                result: rows
            });
        }).catch(function (err) {
            res.json({
                msg: 'error'
            });
        });
    })
    app.post('/api/user-login', (req, res) => {
        //variables
        let mail = req.body.email
        let password = req.body.password

        controllers.login(mail, password).then(function (rows) {
            res.json({
                msg: 'success',
                result: rows
            });
        }).catch(function (err) {
            res.json({
                msg: 'error'
            });
        });
    })


    app.post('/api/icon', (req, res) => {
        //variables
        let name = req.body.name
        let color = req.body.color
        controllers.addIcon(name, color).then(function (rows) {
            res.json({
                msg: 'success',
                result: rows
            });
        }).catch(function (err) {
            res.json({
                msg: 'error'
            });
        });

    })

    app.post('/api/category', (req, res) => {

        //variables
        let cat_name = req.body.cat_name
        let type = req.body.type
        let icon_id = req.body.icon_id
        let user_id = req.body.user_id

        controllers.addCategory(cat_name, type, icon_id, user_id).then(function (rows) {
            res.json({
                msg: 'success',
                result: rows
            });
        }).catch(function (err) {
            res.json({
                msg: 'error'
            });
        });
    })
    app.post('/api/amounts/', (req, res) => {
        //variables
        let table = req.body.table
        let cat_id = req.body.cat_id
        let amount = req.body.amount
        let memo = req.body.memo
        let user_id = req.body.user_id

        controllers.addAmount(table, cat_id, amount, memo, user_id).then(function (rows) {
            res.json({
                msg: 'success',
                result: rows
            });
        }).catch(function (err) {
            res.json({
                msg: 'error'
            });
        });

    })
//    GET
    app.get('/api/amounts/user/:user_id', (req, res) => {
        //variables
        let user_id = req.params.user_id

        controllers.getAmount(user_id).then(function (amounts) {

            amounts = amounts.map(function (amountItem) {
                amountItem.time = dateFormat(amountItem.time, 'dd.mm.yyyy')
                return amountItem
            })

            res.render('partials/transactions', {amounts});
        }).catch(function (err) {
            res.json({
                msg: 'error'
            });
        });


    })

    app.get('/api/balance/user/:user_id', (req, res) => {
        //variables
        let user_id = req.params.user_id

        controllers.getBalance(user_id).then(function (rows) {


            res.render('partials/infoBalance', {
                totalExpense:parseInt(rows[0].total),
                totalIncome:parseInt(rows[1].total),
                Balance:parseInt(rows[1].total) - parseInt(rows[0].total),
            });
        }).catch(function (err) {
            res.json({
                msg: 'error'
            });
        });

    })

    app.get('/api/categories/user/:user_id', (req, res) => {
        //variables
        let user_id = req.params.user_id;

        controllers.getCategories(user_id).then(function (rows) {
            res.json({
                msg: 'success',
                result: rows
            });
        }).catch(function (err) {
            res.json({
                msg: 'error'
            });
        });
    });

    app.get('/api/icons', (req, res) => {
        controllers.getCatIcons().then(function (rows) {
            res.json({
                msg: 'success',
                result: rows
            });
        }).catch(function (err) {
            res.json({
                msg: 'error'
            });
        });
    })

}

module.exports = startAPI