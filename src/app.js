const API = require('./api')
const path = require('path')
const express = require('express')
const hbs = require('hbs')
const createError = require('http-errors')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const dateFormat = require('dateformat')
const app = express()

// Define paths for Express config
const publicDP = path.join(__dirname,'../public')
const viewsPath = path.join(__dirname,'../templates/views')
const partialsPath = path.join(__dirname,'../templates/views/partials')

// Setup handlebars engine and views location
app.set('view engine', 'hbs')
app.set('views',viewsPath)
hbs.registerPartials(partialsPath)


// setup static directory to serve
app.use(express.static(publicDP));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(bodyParser.urlencoded({
    extended: true
}));

API(app)

app.get('', (req,res) => {

    res.render('index',{
        user_id:req.cookies['user_id'],
        user_email:req.cookies['user_email'],
    })
})
app.listen(4000,()=>{
    console.log('server is up')
})