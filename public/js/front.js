jQuery(document).ready(function ($) {
    $('.modal').hide()
    showModal('login-btn', 'login-modal')
    showModal('add-btn-main', 'add-modal')
    showModal('category-btn', 'amount-money-modal')


    showModal('expenses-add-btn', 'add-expenses-cat')

    selectTab('login-tab')
    selectTab('signup-tab')
    selectTab('income-tab')
    selectTab('expenses-tab')


    $.ajax({
        url: `/api/balance/user/${$.cookie('user_id')}`,
        type: 'GET',
        dataType: 'text',

        success: function (result) {
            $('.info-balance').html(result);
        }
    })

    $.ajax({
        url: `/api/amounts/user/${$.cookie('user_id')}`,
        type: 'GET',
        dataType: 'text',
        success: function (result) {
            console.log(result)
            $('.transaction').html(result);
        }
    })

    $(document).on('click', '.overlay', function () {
        $('.modal-container').hide()
        $('.modal').hide()
    })


    $(document).on('click', '.back-from-add-exp-cat', function () {
        $('.add-expenses-cat').hide()

    })

//    AJAX Calls
    $(document).on('click', '#user-register-btn', function () {
        let $email = $('#user-email').val()
        let $password = $('#user-password').val()
        if ($.trim($email).length == 0 || $.trim($password).length == 0) {
            console.log($.trim($email))
            console.log($.trim($password.length))
            alert('mail or password is empty')
            return
        }
        $.ajax({
            url: '/api/user-register',
            type: 'POST',
            data: {
                email: $email,
                password: $password,
            },
            success: function (result) {
                alert('Succesfully registered PLease Log in')
                $('#login-form')[0].reset();
                console.log(result)
            }
        })
    })
    $(document).on('click', '#user-login-btn', function () {
        let $email = $('#user-email').val()
        let $password = $('#user-password').val()
        if ($.trim($email).length == 0 || $.trim($password).length == 0) {
            console.log($.trim($email))
            console.log($.trim($password.length))
            alert('mail or password is empty')
            return
        }
        $.ajax({
            url: '/api/user-login',
            type: 'POST',
            data: {
                email: $email,
                password: $password,
            },
            success: function (result) {

                $('#login-form')[0].reset();
                if (result.result.length == 0) {
                    alert('Email Or Password is wrong')
                    return
                }

                $.cookie("user_email", result.result[0].email, {expires: 30, path: '/'})
                $.cookie("user_id", result.result[0].ID, {expires: 30, path: '/'})
                location.reload();

            }
        })
    })

    $(document).on('click', '#insert-cat-amount', function () {
        let $cat_id = $(this).data('cat_id')
        let $table = $(this).parent().find('.category-btn').data('type')
        let $amount = $('#amount-amount').val()
        let $memo = $('#amount-memo').val()

        let $user_id = $.cookie('user_id')

        $.ajax({
            url: '/api/amounts',
            type: 'POST',
            data: {
                cat_id:$cat_id,
                table:$table,
                amount:$amount,
                memo:$memo,
                user_id:$user_id,
            },
            success: function (result) {
                console.log(result.rows)
                alert(result.msg)
            }
        })


    })

    $(document).on('click', '#expense-add-cat-btn', function () {
        if ($('.expense-icons-grid').data('loaded')) return;
        $.ajax({
            url: '/api/icons',
            type: 'GET',
            data: {},
            success: function (result) {
                $('.expense-icons-grid').data('loaded', true)
                $.each(result.result, function (index, value) {

                    addIconstoGRid('expense-icons-grid', value.icon_color, value.icon_name, value.icon_id)
                });
            }
        })
    })
    $(document).on('click', '#icon-submit', function () {
        let name = $('#icon_name').val()
        let color = randomcolors[Math.floor(Math.random() * randomcolors.length)]

        $.ajax({
            url: '/api/icon',
            type: 'POST',
            data: {
                name: name,
                color: color,
            },
            success: function (result) {
                alert('Succes')
                console.log(result)
            }
        })
    })
    $(document).on('click', '#done-expense-cat', function () {
        let $cat_name = $(this).parents('.modal-wrapper').find('.category-name-inp').val()
        let $icon_id = $(this).parents('.modal-wrapper').find('input[name="selected-category-icon"]:checked').val();
        let $type = $(this).parents('.add-expenses-cat').data('type')
        let $user_id = 0

        if ($.cookie('user_id')) {
            $user_id = $.cookie('user_id')
        }
        if ($.trim($cat_name).length == 0 || $.trim($icon_id).length == 0) {
            alert('Please Leave a Category name and select a in')
            return
        }
        $.ajax({
            url: '/api/category',
            type: 'POST',
            data: {
                cat_name: $cat_name,
                icon_id: $icon_id,
                type: $type,
                user_id: $user_id,
            },
            success: function (result) {
                alert('Success')
                console.log(result)
            }
        })

    })

});


//functions
function selectTab(tab) {
    $(document).on('click', '#' + tab, function () {
        if (tab == 'login-tab') {
            $(this).addClass('selected')
            $('#signup-tab').removeClass('selected')
            $('.register-container').removeClass('is-active')
            $('.login-container').addClass('is-active')
        } else if (tab == 'signup-tab') {
            $(this).addClass('selected')
            $('#login-tab').removeClass('selected')
            $('.login-container').removeClass('is-active')
            $('.register-container').addClass('is-active')
        } else if (tab == 'income-tab') {
            $(this).addClass('selected')
            $('#expenses-tab').removeClass('selected')
            $('.expenses-menu').removeClass('is-active')
            $('.income-menu').addClass('is-active')
        } else if (tab == 'expenses-tab') {
            $(this).addClass('selected')
            $('#income-tab').removeClass('selected')
            $('.income-menu').removeClass('is-active')
            $('.expenses-menu').addClass('is-active')
        }

    })
}

function showModal(btn, modal) {


    $(document).on('click', '.' + btn, function () {
        let $user_id = 0
        if ($.cookie('user_id')) {
            $user_id = $.cookie('user_id')
        }

        if (btn == 'expenses-add-btn') {
            let $type = $(this).data('type')
            $('.' + modal).data('type', $type)
            $('#add-cat-modal-title').html('Add ' + $type + ' category')
        }

        $('.modal').show()
        $('.' + modal).show()
        if (modal == 'amount-money-modal') {
            $('.' + modal).css('transform', 'translateY(-100%)')
            $('.' + modal).find('button').data('cat_id', $(this).data('cat_id'))
            $('.' + modal).find('.amount-icon').html($(this).clone())

        }
        if (btn == 'add-btn-main') {
            if ($('.add-btn-main').data('loaded')) return;
            $.ajax({
                url: `/api/categories/user/${$user_id}`,
                type: 'GET',
                success: function (result) {
                    $('.add-btn-main').data('loaded', true)
                    $.each(result.result, function (index, value) {
                        if (value.type == 'expense') {
                            addCategoriestoGrid('expenses-menu', value.ID, value.cat_name, value.icon_name, value.icon_color,'expenses')
                        } else if (value.type == 'income') {
                            addCategoriestoGrid('income-menu', value.ID, value.cat_name, value.icon_name, value.icon_color,'income')
                        }
                    });
                    $('.expenses-menu').children().append(`<div class='cat-cont'>
                        <div class='add-btn-wrapper '>
                            <div class="add-btn fs-3 icon expenses-add-btn" data-type="expense"
                                 id="expense-add-cat-btn">
                                +
                            </div>
                            <span class="cat-name ">Add</span>
                        </div>
                    </div>`)
                    $('.income-menu').children().append(`<div class='cat-cont'>
                    <div class='add-btn-wrapper '>
                        <div class="add-btn fs-3 icon expenses-add-btn" data-type="income" id="expense-add-cat-btn" >
                            +
                        </div>
                        <span  class="cat-name " >Add</span>
                    </div>
                </div>`)
                }
            })
        }
    })
}

function addIconstoGRid(grid, color, name, id) {
    $('.' + grid).append(`<div class="cat-cont">
  <input type="radio" name="selected-category-icon" value="${id}" id="${name}"  />
                            <label for="${name}">
                          <div class="icon" style="background: ${color}">
                          
                            <span class="material-icons-outlined">${name}</span>
                            </div>
                               </label>
                          </div>`)
}

function addCategoriestoGrid(menu, cat_id, cat_name, icon_name, icon_color,type) {
    $('.' + menu).children().append(`<div class="cat-cont" > 
        <div class="icon category-btn" data-cat_id="${cat_id}" data-type="${type}" style="background:${icon_color}"> 
        <span class="material-icons-outlined">${icon_name}</span> 
        </div>
        <span  class="cat-name" >${cat_name}</span> 
        </div>`
    )
}

let randomcolors = [
    '#934666',
    '#587380',
    '#6565F3',
    '#764054',
    '#95E756',
    '#B7554D',
    '#9B7DF4',
    '#CA14BB',
    '#2EDC54',
    '#0BFFC2',
    '#C4DFD0',
    '#FC4902',
    '#9EE790',
    '#EA57EE',
    '#0C447C',
    '#31DD01',
    '#BE8221',
    '#F60D9F',
]
